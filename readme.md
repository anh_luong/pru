Ask Anh

-bone Kernel

```
git clone https://github.com/beagleboard/am335x_pru_package.git
export CROSS_COMPILE=
cd pru_sw/app_loader/interface
make
```

```
cd pru_sw/utils
mv pasm pasm_linuxintel
cd pasm_source
source ./linux_build
```

```
cp libprussdrv.a /usr/lib/.
cd ../include
mkdir pruss
cp *.h /usr/include/pruss/
cd pru_sw/utils
cp pasm /usr/bin
```

### PRU Debugging ###
```
git clone git@github.com:SPAN-UofU/prudebug.git
cd prudebug
make
cp prudebug /export/rootfs/usr/bin/
```