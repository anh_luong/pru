#!/bin/bash

echo "Compiling the overlay from .dts to .dtbo"

dtc -O dtb -o CC1200-PRU-00A0.dtbo -b 0 -@ CC1200-PRU-00A0.dts
dtc -O dtb -o CC1200-SPIDEV-00A0.dtbo -b 0 -@ CC1200-SPIDEV-00A0.dts
dtc -O dtb -o BB-ADC-00A0.dtbo -b 0 -@ BB-ADC-00A0.dts
dtc -O dtb -o XO-SPIDEV-00A0.dtbo -b 0 -@ XO-SPIDEV-00A0.dts
sudo mv *.dtbo /export/rootfs/lib/firmware
