.origin 0
.entrypoint START

#include "PRU0SPI.hp"

#define SPI0_CLKCTRL    0x44E0004C
#define SPI0_SYSCONFIG  0x48030110
#define SPI0_SYSSTATUS  0x48030114
#define SPI0_IRQSTATUS  0x48030118
#define SPI0_IRQENABLE  0x4803011C
#define SPI0_SYST       0x48030124
#define SPI0_MODULCTRL  0x48030128
#define SPI0_CH0CONF    0x4803012C 
#define SPI0_CH0STAT    0x48030130
#define SPI0_CH0CTRL    0x48030134
#define SPI0_TX0        0x48030138
#define SPI0_RX0        0x4803013C

#define SPI1_CLKCTRL    0x44E00050
#define SPI1_SYSCONFIG  0x481A0110
#define SPI1_SYSSTATUS  0x481A0114
#define SPI1_IRQSTATUS  0x481A0118
#define SPI1_IRQENABLE  0x481A011C
#define SPI1_SYST       0x481A0124
#define SPI1_MODULCTRL  0x481A0128
#define SPI1_CH0CONF    0x481A012C 
#define SPI1_CH0STAT    0x481A0130
#define SPI1_CH0CTRL    0x481A0134
#define SPI1_TX0        0x481A0138
#define SPI1_RX0        0x481A013C

#define MAP_CONST       0x00000000

START:
    // Enable OCP master port
    LBCO r0, CONST_PRUCFG, 4, 4
    CLR  r0, r0, 4
    SBCO r0, CONST_PRUCFG, 4, 4

    // C28 will point to 0x00010000 (PRU shared RAM)
    MOV  r0, 0x00000100
    MOV  r1, CTPPR_0
    ST32 r0, r1
    
    // Disable channel 0 of MCSPI0
    CALL DISABLE_SPI0_CH0
    
    // Disable channel 0 of MCSPI1
    CALL DISABLE_SPI1_CH0

    // Start
    SET r30.t7

    // Initialize sampling counter
    LBCO r5, CONST_PRUSHAREDRAM, 0, 4

SMP:
    // Initialize word counter
    MOV  r3, 0

    // MEM address counter
    MOV  r6, 0

    // Last phase
    MOV  r7, 0

    // Wait until sample is ready
    WBS r31.t4

    // Configure channel 0 of MCSPI1
    CALL SET_SPI1_SPIEN0

// ******************** BEGIN READING IQ SAMPLE ********************
SPI1_TX:    
    // Enable channel 0 of MCSPI1
    CALL ENABLE_SPI1_CH0
    
    // Make sure SPI1_TX0 reg is cleared
    CALL CHECK_SPI1_TX0

    // Write word to SPI1_TX0 register
    MOV  r1, SPI1_TX0
    MOV r4, 0xef
    QBEQ ELSE, r3, 0 
    MOV r4, 0x81
    QBEQ ELSE, r3, 1
    MOV r4, 0x00
ELSE:
    SBBO r4, r1, 0, 4

    // Check if SPI1_RX0 done
    CALL CHECK_SPI1_RX0

SPI1_RX:
    // Get data from SPI1_RX0 Reg
    MOV  r1, SPI1_RX0
    LBBO r2, r1, 0, 4

    // Store
    SBCO r2, CONST_PRUSHAREDRAM, r6, 1 //byte to receive

    // Increment
    ADD  r6, r6, 1

    // Check channel 0 of MCSPI1
    MOV  r1, SPI1_CH0STAT
    LBBO r2, r1, 0, 4
    QBBS SPI1_RX, r2.t0

END_SPI1_TX0:
    // Disable channel 0 of MCSPI1
    CALL DISABLE_SPI1_CH0

    // Increment counter 
    ADD r3, r3, 1
    QBNE SPI1_TX, r3, 7

    // Configure channel 0 of MCSPI1
    CALL CLR_SPI1_SPIEN0

// ******************** END READING IQ SAMPLE ********************
    
    LBCO r1, CONST_PRUSHAREDRAM, 5, 1 // ANG1
    LBCO r2, CONST_PRUSHAREDRAM, 6, 1 // ANG0

    LSL  r1, r1, 8 // Shift left 8 bits
    ADD  r1, r1, r2

    // Compute phase different
    SUB  r2, r7, r1

    // Wrapping
    

// ******************** BEGIN ADJUST XO ********************
    // Configure channel 0 of MCSPI0
    CALL SET_SPI0_SPIEN0

    // XO DAC A
    // Enable channel 0 of MCSPI0
    CALL ENABLE_SPI0_CH0

    // Wait until completed
    CALL CHECK_SPI0_EOT0

    // Write word to SPI0_TX0 register
    MOV  r1, SPI0_TX0
    MOV  r4, 0x3FFF
    SBBO r4, r1, 0, 4

    // Disable channel 0 of MCSPI0
    CALL DISABLE_SPI0_CH0

    // XO DAC B
    // Enable channel 0 of MCSPI0
    CALL ENABLE_SPI0_CH0

    // Wait until completed
    CALL CHECK_SPI0_EOT0

    // Write word to SPI0_TX0 register
    MOV  r1, SPI0_TX0
    MOV  r4, 0xBFFF
    SBBO r4, r1, 0, 4

    // Disable channel 0 of MCSPI0
    CALL DISABLE_SPI0_CH0

    // Configure channel 0 of MCSPI0
    CALL CLR_SPI0_SPIEN0
// ******************** END ADJUST XO ********************

END_SMP:
    // Increment counter
    SUB r5, r5, 1
    QBEQ EXIT, r5, 0
    JMP SMP

/////////// SPI0 ///////////
CHECK_SPI0_TX0:
    // Check channel 0 of MCSPI0
    MOV  r1, SPI0_CH0STAT
    LBBO r2, r1, 0, 4
    QBBS CHECK_SPI0_TX0, r2.t1
    RET

CHECK_SPI0_RX0:
    // Check channel 0 of MCSPI0
    MOV  r1, SPI0_CH0STAT
    LBBO r2, r1, 0, 4
    QBBC CHECK_SPI0_RX0, r2.t0
    RET

CHECK_SPI0_EOT0:
    // Check channel 0 of MCSPI0
    MOV  r1, SPI0_CH0STAT
    LBBO r2, r1, 0, 4
    QBBC CHECK_SPI1_EOT0, r2.t2
    RET

DISABLE_SPI0_CH0:
    //Disable channel 0 of MCSPI0
    MOV  r1, SPI0_CH0CTRL 
    MOV  r2, 0x00000000
    SBBO r2, r1, 0, 4
    RET

ENABLE_SPI0_CH0:
    // Enable channel 0 of MCSPI0
    MOV  r1, SPI0_CH0CTRL 
    MOV  r2, 0x00000001
    SBBO r2, r1, 0, 4
    RET

CONF_SPI0_CH0:
    MOV  r1, SPI0_CH0CONF 
    MOV  r2, 0x000127C8
    SBBO r2, r1, 0, 4
    RET

CLR_SPI0_SPIEN0:
    // Configure channel 0 of MCSPI0
    MOV  r1, SPI0_CH0CONF 
    MOV  r2, 0x000127C8
    CLR  r2.t20 
    SBBO r2, r1, 0, 4
    RET

SET_SPI0_SPIEN0:
    // Configure channel 0 of MCSPI0
    MOV  r1, SPI0_CH0CONF 
    MOV  r2, 0x000127C8
    SET  r2.t20 
    SBBO r2, r1, 0, 4
    RET

/////////// SPI1 ///////////
CHECK_SPI1_TX0:
    // Check channel 0 of MCSPI1
    MOV  r1, SPI1_CH0STAT
    LBBO r2, r1, 0, 4
    QBBC CHECK_SPI1_TX0, r2.t1
    RET

CHECK_SPI1_RX0:
    // Check channel 0 of MCSPI1
    MOV  r1, SPI1_CH0STAT
    LBBO r2, r1, 0, 4
    QBBC CHECK_SPI1_RX0, r2.t0
    RET

CHECK_SPI1_EOT0:
    // Check channel 0 of MCSPI1
    MOV  r1, SPI1_CH0STAT
    LBBO r2, r1, 0, 4
    QBBC CHECK_SPI1_EOT0, r2.t2
    RET

DISABLE_SPI1_CH0:
    //Disable channel 0 of MCSPI1
    MOV  r1, SPI1_CH0CTRL
    MOV  r2, 0x00000000
    SBBO r2, r1, 0, 4
    RET

ENABLE_SPI1_CH0:
    // Enable channel 0 of MCSPI1
    MOV  r1, SPI1_CH0CTRL
    MOV  r2, 0x00000001
    SBBO r2, r1, 0, 4
    RET

CLR_SPI1_SPIEN0:
    // Configure channel 0 of MCSPI1
    MOV  r1, SPI1_CH0CONF 
    MOV  r2, 0x000103CC
    CLR  r2.t20 
    SBBO r2, r1, 0, 4
    RET

SET_SPI1_SPIEN0:
    // Configure channel 0 of MCSPI1
    MOV  r1, SPI1_CH0CONF 
    MOV  r2, 0x000103CC
    SET  r2.t20 
    SBBO r2, r1, 0, 4
    RET

//////////////////////////////////////
EXIT:
    // End
    CLR r30.t7

    MOV R31.b0, PRU0_ARM_INTERRUPT+16
    HALT

