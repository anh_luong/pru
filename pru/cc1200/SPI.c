// Standard header files
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#include <linux/types.h>

#include <pruss/prussdrv.h>
#include <pruss/pruss_intc_mapping.h>
#include "SPI_bin.h"

/******************************************************************************
* Local Macro Declarations                                                    * 
******************************************************************************/
#define PRU_NUM 	0
#define OFFSET_SHAREDRAM 0
//#define PRUSS0_SHARED_DATARAM    4

/******************************************************************************
* Global variable Declarations                                                * 
******************************************************************************/
static void *sharedMem;
static unsigned char *sharedMem_char;

struct IQSample {
    unsigned char magn2;
    unsigned char magn1;
    unsigned char magn0;
    unsigned char ang1;
    unsigned char ang0;
} sample;

/******************************************************************************
* Main                                                                        * 
******************************************************************************/
int main (int argc, char* argv[])
{
	unsigned int ret;
	tpruss_intc_initdata pruss_intc_initdata = PRUSS_INTC_INITDATA;
    struct timeval start, end;
    long mtime, seconds, useconds;   

	/* Initializing PRU */
	prussdrv_init();
	ret = prussdrv_open(PRU_EVTOUT_0);
	if (ret){
    	printf("\tERROR: prussdrv_open open failed\n");
    	return (ret);
	}

    prussdrv_pruintc_init(&pruss_intc_initdata);
    printf("\tINFO: Initializing.\r\n");
    prussdrv_map_prumem(PRUSS0_SHARED_DATARAM, &sharedMem);
	
    sharedMem_char = (unsigned char *) sharedMem;
    sharedMem_char[OFFSET_SHAREDRAM + 0] = 0xef; //0xaf;
    sharedMem_char[OFFSET_SHAREDRAM + 1] = 0x8f; //0x8f;
    sharedMem_char[OFFSET_SHAREDRAM + 2] = 0x3d; //0xaf;
    sharedMem_char[OFFSET_SHAREDRAM + 3] = 0x3d; //0x90;
    sharedMem_char[OFFSET_SHAREDRAM + 4] = 0x3d; //0x3d;
    sharedMem_char[OFFSET_SHAREDRAM + 5] = 0x3d; //0x3d;
    sharedMem_char[OFFSET_SHAREDRAM + 6] = 0x3d; //0x3d;

    printf("\tINFO: TX %02x %02x %02x %02x %02x %02x %02x\n", sharedMem_char[OFFSET_SHAREDRAM + 0], \
                                                              sharedMem_char[OFFSET_SHAREDRAM + 1], 
                                                              sharedMem_char[OFFSET_SHAREDRAM + 2],
                                                              sharedMem_char[OFFSET_SHAREDRAM + 3],
                                                              sharedMem_char[OFFSET_SHAREDRAM + 4],
                                                              sharedMem_char[OFFSET_SHAREDRAM + 5],
                                                              sharedMem_char[OFFSET_SHAREDRAM + 6]);
    
    /* Executing PRU. */
    //prussdrv_exec_program (PRU_NUM, "ADCCollector_bin.h");
    prussdrv_pru_write_memory(PRUSS0_PRU0_IRAM, 0, PRUcode, sizeof(PRUcode));
    prussdrv_pru_enable(0);

    gettimeofday(&start, NULL);

    printf("\tINFO: RX %02x %02x %02x %02x %02x %02x %02x\n", sharedMem_char[OFFSET_SHAREDRAM + 0], \
                                                              sharedMem_char[OFFSET_SHAREDRAM + 1], 
                                                              sharedMem_char[OFFSET_SHAREDRAM + 2],
                                                              sharedMem_char[OFFSET_SHAREDRAM + 3],
                                                              sharedMem_char[OFFSET_SHAREDRAM + 4],
                                                              sharedMem_char[OFFSET_SHAREDRAM + 5],
                                                              sharedMem_char[OFFSET_SHAREDRAM + 6]);

    //printf("\tINFO: PRU completed transfer.\r\n");
    //prussdrv_pru_clear_event (PRU_EVTOUT_0, PRU0_ARM_INTERRUPT);

    ret = prussdrv_pru_wait_event(PRU_EVTOUT_0);

    gettimeofday(&end, NULL);

    seconds  = end.tv_sec  - start.tv_sec;
    useconds = end.tv_usec - start.tv_usec;
    mtime = (seconds + (useconds/1000000));

    printf("PRU program completed (%d), in %d seconds \n", ret, (int)mtime);

    /* Disable PRU*/
    prussdrv_pru_disable(PRU_NUM);
    prussdrv_exit();

    return(0);
}
